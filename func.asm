;I will refer to color as 0 for black and 1 for anything else
;So red and green are both 1 and are same color in this code

section	.text
global find_markers

find_markers:
    push ebp
    mov ebp, esp ;ebp holds now base pointer
    ; ebp+8 bmp array pointer
    ; ebp+12 x array pointer
    ; ebp+16 y array pointer
    ; Extra reserving space for local variables
    ; ebp-4 height
    ; ebp-8 width
    ; ebp-12 line size
    ; ebp -16 offset
    ; ebp -20 start of pixel array
    sub esp, 16 ;reserving space for extra variables
    
    push ebx

    mov ebx, [ebp+8] ;pixel array pointer

    push ebx ;arg1 = bmp
    call is_bmp
    pop ebx ;pop argument from stack
    mov ecx, eax
    mov eax, -1
    cmp ecx, 0 ;check if is bmp
    jne quit_function ;if not quit
    
    push ebx ;pushing argument
    call get_height ;checking height
    pop ebx
    mov ecx, eax
    mov [ebp-4], eax ;storing result on stack
    mov eax, -1
    cmp ecx, 0 ;check if height is positive
    jle quit_function ;if not quit

    push ebx ;pushing argument
    call get_width ;checking width
    pop ebx
    mov ecx, eax
    mov [ebp-8], eax ;storing result on stack
    mov eax, -1
    cmp ecx, 0 ;check if width is positive
    jle quit_function ;if not quit

    mov ebx, [ebp+8]
    push ebx ;pushing argument
    call get_offset ;checking offset
    pop ebx
    mov [ebp-16], eax ;storing result on stack

    mov ecx, [ebp+8] ;load start of bmp
    add ecx, [ebp-16] ;bmp start + offset = first pixel
    mov [ebp-20], ecx ;save first pixel location

    mov ebx, [ebp-8] ;loading width argument
    push ebx ;pushing argument to stack
    call get_line_size ;getting line width
    pop ebx
    mov [ebp-12], eax ;storing result

    push DWORD [ebp+16] ;load y array pointer
    push DWORD [ebp+12] ;load x array pointer
    push DWORD [ebp-12] ;load line size argument
    push DWORD [ebp-20] ;load first pixel argument
    push DWORD [ebp-8] ;load max width argument
    push DWORD [ebp-4] ;load max height argument
    call start_main_loop_y
    add esp, 24 ;pop arguments
    ;in eax should be result

quit_function:
    pop ebx
    add esp, 16 ;removing extra variables

    pop ebp
    ret

is_bmp:
    ;Arguments:
    ; +8 bmp array
    ;Returns 1 if is not bmp file and 0 if is
    push ebp
    mov ebp, esp
    push ebx
    push ecx

    mov eax, 1
    mov ebx, [ebp+8]
    mov cl, [ebx] ;loading first byte of header
    cmp cl, 0x42 ;checking if is 42
    jne quit_is_bmp
    mov cl, [ebx+1] ;loading second byte of header
    cmp cl, 0x4D ;checking if is 4D
    jne quit_is_bmp
    mov eax, 0

quit_is_bmp:
    pop ecx
    pop ebx
    pop ebp
    ret

get_height:
    ;Arguments:
    ; +8 bmp array
    ;Returning height in eax
    push ebp
    mov ebp, esp ;ebp holds now base pointer

    mov eax, [ebp+8] ;set start of array to eax
    mov eax, [eax+22] ;eax = height

    pop ebp
    ret

get_width:
    ;Arguments:
    ; +8 bmp array
    ;Returning width in eax
    push ebp
    mov ebp, esp ;ebp holds now base pointer

    mov eax, [ebp+8] ;set start of array to eax
    mov eax, [eax+18] ;eax = width

    pop ebp
    ret

get_offset:
    ;Arguments:
    ; +8 bmp array
    ;Returning offset in eax
    push ebp
    mov ebp, esp ;ebp holds now base pointer

    mov eax, [ebp+8] ;set start of array to eax
    mov eax, [eax+10] ;eax = offset

    pop ebp
    ret

get_line_size:
    ;Arguments:
    ; +8 width
    ;Returning line size in eax
    push ebp
    mov ebp, esp ;ebp holds now base pointer
    push ebx

    mov ecx, [ebp+8] ;moving width to ecx
    shl ecx, 1
    add ecx, [ebp+8]
    mov eax, ecx ;moving width to eax for division
    mov edx, 0
    mov ebx, 4
    div ebx ;div to get rest from division
    ;so we can check if we need to increase line size to next number divisible by 4
    mov ecx, edx ;edx will change, so moving result
    mul ebx ;in eax we have div result, so by multiplication by 4 we will get
    ;size of line divisible by 4 equal or smaller (by less than 4) than real line size
    cmp ecx, 0 ;check if mod == 0
    je quit_get_line_size
    add eax, 4 ;if mod % 4 != 0 we add 4

quit_get_line_size:
    pop ebx
    pop ebp
    ret

get_pixel:
    ;Arguments:
    ; +8 max height
    ; +12 max width
    ; +16 start of pixel array
    ; +20 size of line
    ; +24 x
    ; +28 y
    ;Returning 0 if black 1 if not black or out of bmp file
    push ebp
    mov ebp, esp ;ebp holds now base pointer
    
    mov eax, 1 ;placing non black color
    mov ecx, [ebp+24] ;ecx = x
    cmp ecx, [ebp+12] ;x >= width
    jge quit_get_pixel

    cmp ecx, 0
    jl quit_get_pixel ;x < 0

    mov ecx, [ebp+28] ;ecx = y
    cmp ecx, [ebp+8] ;y >= height
    jge quit_get_pixel

    cmp ecx, 0 ;y < 0
    jl quit_get_pixel
    
    mov ecx, [ebp+8] ;ecx = max height
    mov edx, [ebp+28] ;edx = y
    sub ecx, edx ;y = max height - y
    dec ecx ;ecx--
    ;since top is at bottom

    mov eax, [ebp+20] ;eax = size of line
    mul DWORD ecx ;size of line * y
    
    mov ecx, [ebp+24] ;ecx = x
    shl ecx, 1
    add ecx, [ebp+24] ;ecx = 3x
    add eax, ecx ;size of line *y + 3x

    add eax, [ebp+16] ;adress of pixel
    mov ecx, 0 ;zeroing register
    mov cl, [eax] ;loading first color
    inc eax
    mov ch, [eax] ;loading second color
    inc eax
    shl ecx, 8 ;moving one color to the left
    mov cl, [eax] ;loading third color

    mov eax, 0
    cmp ecx, 0
    je quit_get_pixel ;checking if is black
    mov eax, 1

quit_get_pixel:
    pop ebp
    ret

start_main_loop_y:
    ;Arguments:
    ; +8 max height
    ; +12 max width
    ; +16 start of pixel array
    ; +20 size of line
    ; +24 x array pointer
    ; +28 y array pointer
    ;Returning nothing
    push ebp
    mov ebp, esp ;ebp holds now base pointer
    ;reserving extra space
    ; -4 markers found
    sub esp, 4
    push ebx
    
    mov ebx, 0 ;i = 0
    mov DWORD [ebp-4], 0 ;markers found = 0

main_loop_y:
    mov eax, [ebp-4]
    cmp ebx, [ebp+8]
    jge end_main_loop_y ;i < max height to continue

    mov ecx, ebp
    sub ecx, 4 ;load pointer position to ebp 
    push ecx ;load pointer to markers found
    push DWORD [ebp+28] ;load y array
    push DWORD [ebp+24] ;load x array
    push ebx ;load y argument
    push DWORD [ebp+20] ;size of line argument
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;argument max width
    push DWORD [ebp+8] ;load max height as argument
    call start_main_loop_x
    add esp, 32 ;pop arguments

    inc ebx ;i++
    jmp main_loop_y

end_main_loop_y:
    pop ebx
    add esp, 4
    pop ebp
    ret

start_main_loop_x:
    ;Arguments:
    ; +8 max height
    ; +12 max width
    ; +16 start of pixel array
    ; +20 size of line
    ; +24 y
    ; +28 x array pointer
    ; +32 y array pointer
    ; +36 pointer to markers found
    ;Returning nothing
    push ebp
    mov ebp, esp ;ebp holds now base pointer
    push ebx
    mov ebx, 0 ;j = 0

main_loop_x:
    cmp ebx, [ebp+12]
    jge end_main_loop_x ;j < max width to continue

    ;checking current pixel color
    push DWORD [ebp+24] ;y
    push ebx ;x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call get_pixel
    add esp, 24 ;pop arguments
    cmp eax, 0
    jne next_main_loop_x ;if pixel is not black then check next pixel
    
    push DWORD [ebp+36] ;pointer to markers found
    push DWORD [ebp+32] ;y array
    push DWORD [ebp+28] ;x array
    push DWORD [ebp+24] ;y
    push ebx ;x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call check_pixel
    add esp, 36 ;pop arguments

next_main_loop_x:
    inc ebx ;j++
    jmp main_loop_x

end_main_loop_x:
    pop ebx
    pop ebp
    ret

check_pixel:
    ;Arguments:
    ; +8 max height
    ; +12 max width
    ; +16 start of pixel array
    ; +20 size of line
    ; +24 x
    ; +28 y
    ; +32 x array pointer
    ; +36 y array pointer
    ; +40 pointer to markers found
    ;appending pixel to array if it's marker
    push ebp
    mov ebp, esp ;ebp holds now base pointer
    ;Reserving extra space for variables
    sub esp, 12
    ; -4 arms length
    ; -8 diagonal pixel x
    ; -12 diagonal pixel y
    push ebx

    mov ecx, [ebp+24] ;ecx = x
    inc ecx ;x++

    push DWORD [ebp+28] ;y
    push ecx ;x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call get_pixel
    add esp, 24 ;pop arguments
    cmp eax, 0
    je quit_check_pixel ;pixel is black not valid marker

    ;checking pixel below
    mov ecx, [ebp+28] ;ecx = y
    inc ecx ;y++

    push ecx ;y
    push DWORD [ebp+24] ;x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call get_pixel
    add esp, 24 ;pop arguments
    cmp eax, 0
    je quit_check_pixel ;pixel is black not valid marker

    ;left and bottom pixel not black, potentially marker conrner
    push DWORD [ebp+28] ;y
    push DWORD [ebp+24] ;x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call start_arms
    add esp, 24 ;pop arguments

    cmp eax, 0
    jle quit_check_pixel ;not marker, leave

    mov [ebp-4], eax ;saving arms length

    ;now looping through diagonal pixels as long as they are black
    mov ebx, 1 ;i = 1

check_pixel_internal_loop:
    cmp ebx, [ebp-4]
    je quit_check_pixel ;if i==arm length exit

    ; Checking diagonal pixel
    mov ecx, [ebp+28] ;loading y
    sub ecx, ebx ;y-currently checked pixel
    mov [ebp-12], ecx ;storing for future
    push ecx ;y

    mov ecx, [ebp+24] ;loading x
    sub ecx, ebx ;x-currently checked pixel
    mov [ebp-8], ecx ;storing for future
    push ecx ;x

    ;we pushed diagonal pixel location now
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call get_pixel
    add esp, 24 ;pop arguments
    cmp eax, 0
    jne after_checking_black_pixels ;checking if pixel is black, if no continue

    ;sub marker arm length
    mov ecx, [ebp-4] ;loading length to ecx
    sub ecx, ebx ;length - i
    inc ecx ;+1 to check white field at the end
    push ecx ;length to check
    push DWORD [ebp-12] ;diagonal y
    push DWORD [ebp-8] ;diagonal x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call verify_black_arm
    add esp, 28 ;pop arguments
    cmp eax, 0
    je quit_check_pixel ;arms not correct, quit pixel checking

    inc ebx ;i++
    jmp check_pixel_internal_loop
after_checking_black_pixels:
    ;checking internal margin
    ;ebx should be increased from previous stage
    mov ecx, [ebp-4] ;loading length to ecx
    sub ecx, ebx ;length - i
    cmp ecx, 0
    je quit_check_pixel ;it is black square, quit pixel check

    push ecx ;length to check
    push DWORD [ebp-12] ;y
    push DWORD [ebp-8] ;x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call verify_margin ;veryfing internal margin
    add esp, 28 ;pop arguments
    cmp eax, 0
    je quit_check_pixel ;if not correct quit

    ;checking extrenal margin now
    mov ecx, [ebp+28] ;loading y
    inc ecx ;y++ pixel below
    mov edx, [ebp+24] ;loading x
    inc edx ;x++ pixel on right
    push DWORD [ebp-4] ;length to check
    push ecx ;y
    push edx ;x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call verify_margin ;veryfing internal margin
    add esp, 28 ;pop arguments
    cmp eax, 0
    je quit_check_pixel ;if not correct quit

    ;now we confirmed that it is correct marker
    mov ecx, [ebp+40] ;load pointer
    mov ebx, [ecx] ;load value of found markers
    mov edx, [ebp+32] ;load x array
    mov eax, [ebp+24]
    mov [edx+4*ebx], eax ;save x to array
    mov edx, [ebp+36] ;load y array
    mov eax, [ebp+28]
    mov [edx+4*ebx], eax ;save y to array
    inc DWORD [ecx] ;increase number of markers found

quit_check_pixel:
    pop ebx
    add esp, 12
    pop ebp
    ret

start_arms:
    ;Arguments:
    ; +8 max height
    ; +12 max width
    ; +16 start of pixel array
    ; +20 size of line
    ; +24 x
    ; +28 y
    ;appending pixel to array if it's marker
    push ebp
    mov ebp, esp ;ebp holds now base pointer
    sub esp, 4 ;space for extra variables
    ; -4 tmp variable

    push ebx
    mov ebx, 1

arms:
    mov ecx, [ebp+24] ;ecx = x
    sub ecx, ebx ;x - i

    push DWORD [ebp+28] ;y
    push ecx ;x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call get_pixel
    add esp, 24 ;pop arguments
    mov [ebp-4], eax

    mov ecx, [ebp+28] ;ecx = y
    sub ecx, ebx ;y - i

    push ecx ;y
    push DWORD [ebp+24] ;x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call get_pixel
    add esp, 24 ;pop arguments

    mov edx, eax ; edx = color
    mov eax, -1 ;negative value in case if not valid marker
    cmp edx, [ebp-4]
    jne quit_arms ;quit if colors are different

    mov eax, ebx ;load proper value to return
    cmp edx, 0
    jne quit_arms ;exit if color is not black

    inc ebx ;i++
    jmp arms ;continue loop

quit_arms:
    pop ebx
    add esp, 4 ;clear space for extra variables
    pop ebp
    ret

verify_black_arm:
    ;Arguments:
    ; +8 max height
    ; +12 max width
    ; +16 start of pixel array
    ; +20 size of line
    ; +24 x
    ; +28 y
    ; +32 length of arm to check
    ;returns 0 if not correct, 1 if correct
    push ebp
    mov ebp, esp ;ebp holds now base pointer
    ;Reserving extra space
    ; -4 color of pixel above
    sub esp, 4
    push ebx
    mov ebx, 0 ;i = 0

loop_verify_black_arm:
    mov eax, 1 ;if last loop then it is result
    cmp ebx, [ebp+32]
    jge quit_verify_black_arm ;i < arm length

    mov ecx, [ebp+28] ;ecx = y
    sub ecx, ebx ;checking pixel above
    push ecx ;y
    push DWORD [ebp+24] ;x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call get_pixel
    add esp, 24 ;pop arguments
    mov [ebp-4], eax ;storing result

    mov ecx, [ebp+24] ;ecx = y
    sub ecx, ebx ;checking pixel on the left
    push DWORD [ebp+28] ;y
    push ecx ;x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call get_pixel
    add esp, 24 ;pop arguments

    mov edx, eax
    mov eax, 0 ;loading 0 in case if not correct
    cmp edx, [ebp-4] ;checking if pixel above and on the left have same color
    jne quit_verify_black_arm ;if not leave

    mov ecx, 1 ;load not black
    inc ebx ;i++
    cmp ebx, [ebp+32]
    je skip_change_color
    mov ecx, 0 ;load black
skip_change_color:
    mov eax, 0 ;load 0 in case of fail
    cmp edx, ecx
    jne quit_verify_black_arm ;quit if no match

    ;continue checking
    jmp loop_verify_black_arm

quit_verify_black_arm:
    pop ebx
    add esp, 4
    pop ebp
    ret

verify_margin:
    ;Arguments:
    ; +8 max height
    ; +12 max width
    ; +16 start of pixel array
    ; +20 size of line
    ; +24 x
    ; +28 y
    ; +32 length of arm to check
    ;returns 0 if not correct, 1 if correct
    push ebp
    mov ebp, esp ;ebp holds now base pointer
    ;Reserving extra space
    ; -4 color of pixel above
    sub esp, 4
    push ebx
    mov ebx, 0 ;i = 0

loop_verify_margin:
    mov eax, 1 ;if last loop then it is result
    cmp ebx, [ebp+32]
    jge quit_verify_margin ;i < arm length

    mov ecx, [ebp+28] ;ecx = y
    sub ecx, ebx ;checking pixel above
    push ecx ;y
    push DWORD [ebp+24] ;x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call get_pixel
    add esp, 24 ;pop arguments
    mov [ebp-4], eax ;storing result

    mov ecx, [ebp+24] ;ecx = y
    sub ecx, ebx ;checking pixel on the left
    push DWORD [ebp+28] ;y
    push ecx ;x
    push DWORD [ebp+20] ;size of line
    push DWORD [ebp+16] ;start of pixel array
    push DWORD [ebp+12] ;max width
    push DWORD [ebp+8] ;max height
    call get_pixel
    add esp, 24 ;pop arguments

    mov edx, eax
    mov eax, 0 ;loading 0 in case if not correct
    cmp edx, [ebp-4] ;checking if pixel above and on the left have same color
    jne quit_verify_margin ;if not leave

    mov eax, 0 ;load 0 in case of fail
    cmp edx, 0
    je quit_verify_margin ;quit if black

    ;continue checking
    inc ebx ;i++
    jmp loop_verify_margin

quit_verify_margin:
    pop ebx
    add esp, 4
    pop ebp
    ret